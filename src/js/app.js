var button = document.querySelector('#start-button');
var output = document.querySelector('#output');
var output2 = document.querySelector('#output2');

button.addEventListener('click', function() {
  // Create a new Promise here and use setTimeout inside the function you pass to the constructor
  let silka = 'https://swapi.co/api/people/1';
  var promise = new Promise((resolve, reject) => {
    setTimeout(function() { // <- Store this INSIDE the Promise you created!
      // Resolve the following URL: https://swapi.co/api/people/1
      resolve(silka);
    }, 1234);
  });

  // Handle the Promise "response" (=> the value you resolved) and return a fetch()
  // call to the value (= URL) you resolved (use a GET request)
  promise.then(silka => {
    return fetch(silka);
  })
  
   // Handle the response of the fetch() call and extract the JSON data, return that
  // and handle it in yet another then() block

  .then(response => {
    return response.json();
  })

  // Finally, output the "name" property of the data you got back (e.g. data.name) inside
  // the "output" element (see variables at top of the file)

  .then(data => output.innerHTML = data.name)

  // Repeat the exercise with a PUT request you send to https://httpbin.org/put
  // Make sure to set the appropriate headers 
  // Send any data of your choice, make sure to access it correctly when outputting it
  // Example: If you send {person: {name: 'Max', age: 28}}, you access data.json.person.name
  // to output the name (assuming your parsed JSON is stored in "data")
  let yrl = 'https://httpbin.org/put';
  var promis = new Promise((resolve, reject) => {
    setTimeout(() => { // <- Store this INSIDE the Promise you created!
      // Resolve the following URL: https://swapi.co/api/people/1
      resolve(yrl);
    }, 1000);
  });

  promis.then(yrl => {
    return fetch(yrl, {
      method: 'PUT', body: JSON.stringify({person:{ name: 'Jean Pierre Polnareff', age: 20 }})
  });
  })

  .then(response => {
    return response.json();
  })

  .then(data => output2.innerHTML = data.json.person.name)

  .catch(error => {
    console.error('oshibka v otpravke polnareffa')
  })

  // To finish the assignment, add an error to URL and add handle the error both as
  // a second argument to then() as well as via the alternative taught in the module
  let yourl = 'https://httpbin.org/pu';
  
  var proms = new Promise ( (res,rej) => {
    resolve(yourl)
  })

  .then( yourl => {
    return fetch(yourl)
  })
  
  .catch(error => {
    console.error('ошибка в урл');
   setTimeout(() => {
    console.log('исправляем..');
   }, 1000); 
    setTimeout(() => {
      console.log('исправили')
    }, 3000);
  })
});